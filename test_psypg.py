#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import psypg
import psycopg2


if __name__ == '__main__':
    d = psypg.PgConfig('testdb', 'db.ini')
    print('PostgreSQL DSN is {}'.format(d.dsn()))
    print('SQLAlchemy URI is {}'.format(d.sa()))
    print('Peewee URI is {}'.format(d.peewee()))
    try:
        dbh = d.get_handle()
        print('Sending i_am_alive notification')
        psypg.pg_notify(dbh, 'i_am_alive')
    except psycopg2.OperationalError:
        print('Connection failed')
